import AES from "crypto-js/aes";
import UTF8 from "crypto-js/enc-utf8";

const USER_KEY = "user_sess";
const TTL = 1800000;
const SUPERHIDDEN_KEY = "ølaksdjfouøbrkjgnkjsewrnbkljdhfg890u324234";

export function setUserSession(userName) {
  const encrypted = AES.encrypt(userName, SUPERHIDDEN_KEY).toString();

  const newUserSessionItem = {
    value: encrypted,
    expiry: new Date().getTime() + TTL,
  };

  localStorage.setItem(USER_KEY, JSON.stringify(newUserSessionItem));
}

export function getUserSession() {
  const now = new Date();
  const fromStore = localStorage.getItem(USER_KEY);

  if (fromStore === "" || !fromStore) {
    return "";
  }

  const item = JSON.parse(fromStore);
  if (item.expiry < now.getTime()) {
    localStorage.removeItem(USER_KEY);
    return "";
  }

  return AES.decrypt(item.value, SUPERHIDDEN_KEY).toString(UTF8);
}

export function userExists(userName) {
  if (getUserSession(userName)) {
    return true;
  }

  return false;
}

export function clearUserSession() {
  localStorage.setItem(USER_KEY, "");
  return "";
}
