import { Redirect } from "react-router-dom";
import { logIn } from "./redux/actions";
import { getUserSession } from "./utils/sessionStoreUtils";

export const requireAuth = (store, Goto) => {
  const userSession = getUserSession();
  if (userSession && userSession !== "") {
    store.dispatch(logIn(userSession));
    return <Goto />;
  }

  return <Redirect to="/" />;
};
