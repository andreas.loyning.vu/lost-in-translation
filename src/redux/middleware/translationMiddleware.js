import { getUser } from "../../components/login/LoginApi";
import {
  addTranslation,
  deleteTranslations,
} from "../../components/translate/TranslateApi";
import { setTranslation, setUser } from "../actions";
import { TRANSLATION } from "../constants";

export const translatorMiddleware = (store) => (next) => async (action) => {
  if (action.type === TRANSLATION.ADD_TO_USER) {
    try {
      const translation = store.getState().translator.translations;
      const user = store.getState().user;
      console.log(translation);
      translation.unshift(action.payload);
      const newTranslationList = await addTranslation(
        translation.slice(0, 10),
        user.id
      );
      store.dispatch(setTranslation(newTranslationList.translations));
    } catch (error) {
      console.error(error.message);
    }
  }

  if (action.type === TRANSLATION.DELETE) {
    try {
      const user = store.getState().user;
      deleteTranslations(user.id);
      const fetchedUser = await getUser(user.name);
      store.dispatch(setUser(fetchedUser));
    } catch (e) {
      console.error(e.message);
    }
  }
  next(action);
};
