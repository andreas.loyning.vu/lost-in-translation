import { USER } from "../constants";
import { createUser, getUser } from "../../components/login/LoginApi";
import { setTranslation, setUser } from "../actions";
import { getUserSession, setUserSession } from "../../utils/sessionStoreUtils";

export const userMiddleware = (store) => (next) => async (action) => {
  // Fetches and sets the current user. Creates new one if not found
  if (action.type === USER.LOGIN) {
    const userSession = getUserSession();
    if (userSession && userSession !== "") {
      try {
        const user = await getUser(userSession);
        store.dispatch(setUser(user));
        store.dispatch(setTranslation(user.translations));
      } catch (error) {
        console.error(error.message);
      }
    } else {
      try {
        const existingUser = await getUser(action.payload);
        if (existingUser) {
          store.dispatch(setUser(existingUser));
          store.dispatch(setTranslation(existingUser.translations));
          return;
        }
        const newUser = await createUser(action.payload);
        if (newUser) {
          store.dispatch(setUser(newUser));
          store.dispatch(setTranslation(newUser.translations));
          return;
        } else {
          throw new Error(
            "Could not find or create user with name: " + action.payload
          );
        }
      } catch (error) {
        console.error(error.message);
      }
    }
  }

  if (action.type === USER.FETCH) {
    try {
      const fetchedUser = await getUser(action.payload);
      store.dispatch(setUser(fetchedUser));
    } catch (error) {}
  }

  if (action.type === USER.SET) {
    setUserSession(action.payload.name);
  }

  next(action);
};
