import { USER, TRANSLATION } from "./constants";

export const setCurrentTranslation = (translation) => {
  return {
    type: TRANSLATION.SET_CURRENT,
    payload: translation,
  };
};

export const addTranslationToUser = (translation) => {
  return {
    type: TRANSLATION.ADD_TO_USER,
    payload: translation,
  };
};

export const setTranslation = (list) => {
  return {
    type: TRANSLATION.SET,
    payload: list,
  };
};

export const deleteTranslations = () => {
  return {
    type: TRANSLATION.DELETE,
  };
};

export const setUser = (user) => {
  return {
    type: USER.SET,
    payload: user,
  };
};

export const fetchUser = (userName) => {
  return {
    type: USER.FETCH,
    payload: userName,
  };
};

export const logIn = (username) => {
  return {
    type: USER.LOGIN,
    payload: username,
  };
};

export const resetUser = () => {
  return {
    type: USER.RESET,
  };
};
