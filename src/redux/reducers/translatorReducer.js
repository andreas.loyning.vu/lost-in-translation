import { TRANSLATION } from "../constants";

const initialState = {
  current: "",
  translations: [],
};

const translatorReducer = (state = initialState, action) => {
  switch (action.type) {
    case TRANSLATION.SET_CURRENT: {
      return {
        ...state,
        current: action.payload,
      };
    }
    case TRANSLATION.DELETE: {
      return {
        ...state,
        current: [],
        translations: [],
      };
    }
    case TRANSLATION.ADD_TO_USER: {
      return {
        ...state,
      };
    }
    case TRANSLATION.SET: {
      return {
        ...state,
        translations: [...action.payload],
      };
    }
    default: {
      return state;
    }
  }
};

export default translatorReducer;
