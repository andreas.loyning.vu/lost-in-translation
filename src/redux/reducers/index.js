import { combineReducers } from "redux";
import translator from "./translatorReducer";
import user from "./userReducer";

export default combineReducers({ translator, user });
