import { USER } from "../constants";

const initialState = {
  id: null,
  name: "",
  translations: [],
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER.SET: {
      return action.payload;
    }
    case USER.RESET: {
      return initialState;
    }
    case USER.FETCH: {
      return state;
    }
    default: {
      return state;
    }
  }
};

export default userReducer;
