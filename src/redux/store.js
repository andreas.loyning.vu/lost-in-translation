import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension";
import { userMiddleware } from "./middleware/userMiddleware";
import { translatorMiddleware } from "./middleware/translationMiddleware";

export default createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(userMiddleware, translatorMiddleware))
);
