import { useSelector } from "react-redux";
import { Table } from "semantic-ui-react";
import styles from "./UserProfile.module.css";

export default function UserTranslations() {
  // TODO: Use correct suff
  const user = useSelector((state) => state.user);
  const specialChars = /[^A-Z]/i;

  return (
    <div className={styles.Table}>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>From</Table.HeaderCell>
            <Table.HeaderCell>To</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {user.translations &&
            user.translations.map((translation, index) => {
              return (
                <Table.Row key={index}>
                  <Table.Cell>
                    <h3>{translation}</h3>
                  </Table.Cell>
                  <Table.Cell>
                    {translation.split("").map((char, index) => {
                      if (char.match(specialChars)) {
                        return null;
                      }
                      return (
                        <img
                          src={`/resources/individial_signs/${char}.png`}
                          alt={char}
                          key={translation + char + index}
                        />
                      );
                    })}
                  </Table.Cell>
                </Table.Row>
              );
            })}
        </Table.Body>
      </Table>
    </div>
  );
}
