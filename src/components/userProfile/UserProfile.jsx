import { Grid } from "semantic-ui-react";
import styles from "./UserProfile.module.css";
import { useDispatch } from "react-redux";
import UserTranslations from "./UserTranslations";
import { deleteTranslations, resetUser } from "../../redux/actions";
import { clearUserSession } from "../../utils/sessionStoreUtils";
import { useHistory } from "react-router-dom";

export default function UserProfile(params) {
  const dispatch = useDispatch();
  const history = useHistory();
  function onDeleteButtonClicked() {
    dispatch(deleteTranslations());
  }

  function onLogOutClicked() {
    dispatch(resetUser());
    clearUserSession();
    history.replace("/");
  }
  return (
    <>
      <Grid centered>
        <Grid.Row>
          <h2 className={styles.Header}>Your last 10 translations</h2>
        </Grid.Row>
        <Grid.Row>
          <button
            className={styles.DeleteButton}
            onClick={onDeleteButtonClicked}
          >
            DELETE TRANSLATIONS
          </button>
          <button className={styles.LogoutButton} onClick={onLogOutClicked}>
            LOG OUT
          </button>
        </Grid.Row>
        <Grid.Row>
          <UserTranslations />
        </Grid.Row>
      </Grid>
    </>
  );
}
