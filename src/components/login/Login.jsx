import LoginInput from "./LoginInput";
import { Grid } from "semantic-ui-react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { getUserSession } from "../../utils/sessionStoreUtils";
import styles from "./Login.module.css";

export default function Login() {
  const user = useSelector((state) => state.user);
  const userSession = getUserSession();
  return (
    <>
      {user.name && userSession !== "" && <Redirect to="/translator" />}
      <Grid centered>
        <Grid.Row>
          <h1 className={styles.Header}>Lost in Translation</h1>
        </Grid.Row>
        <Grid.Row>
          <div className={styles.Grid}>
            <LoginInput />
          </div>
        </Grid.Row>
      </Grid>
    </>
  );
}
