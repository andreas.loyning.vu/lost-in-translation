import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUserSession } from "../../utils/sessionStoreUtils";
import { logIn } from "../../redux/actions";
import styles from "./Login.module.css";

export default function LoginInput() {
  const [name, setName] = useState("");
  const user = useSelector((state) => state.user);
  const userSession = getUserSession();
  const dispatch = useDispatch();

  useEffect(() => {
    if (!user.name && userSession !== "") {
      dispatch(logIn(userSession.name));
    }
  });
  function onChanged(e) {
    setName(e.target.value);
  }

  // Sets the user if found, creates and sets new one if not.
  function onLoginClicked() {
    dispatch(logIn(name));
  }

  return (
    <div className={styles.InputField}>
      <div className={styles.KeyboardIcon}>
        <span class="material-icons">keyboard</span>
      </div>
      <input
        placeholder="What's your name?"
        onChange={onChanged}
        value={name}
        className={styles.Input}
      ></input>
      <button onClick={onLoginClicked} className={styles.LoginButton}>
        <span class="material-icons">keyboard_arrow_right</span>
      </button>
    </div>
  );
}
