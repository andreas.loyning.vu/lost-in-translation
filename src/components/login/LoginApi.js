const BASE_URL = "http://localhost:5000";

export async function getUser(username) {
  try {
    const response = await fetch(`${BASE_URL}/users?name=${username}`);
    const body = await response.json();
    if (body != null) {
      console.log("body is here!");
      return body.pop();
    }
  } catch (e) {
    return handleError(e);
  }
}

export async function createUser(username) {
  try {
    const response = await fetch(`${BASE_URL}/users/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ name: username, translations: [] }),
    });
    return await response.json();
  } catch (e) {
    return handleError(e);
  }
}

const handleError = (e) => {
  return e.message;
};
