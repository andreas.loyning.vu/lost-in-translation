const BASE_URL = "http://localhost:5000";

export async function addTranslation(translation, userID) {
  try {
    // current translations will be stored in redux
    // temporarily not used
    const postbody = { translations: translation };
    const response = await fetch(`${BASE_URL}/users/${userID}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(postbody),
    });
    return await response.json();
  } catch (e) {
    return e.message;
  }
}

export async function deleteTranslations(userID) {
  try {
    const response = await fetch(`${BASE_URL}/users/${userID}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ translations: [] }),
    });
    const json = await response.json();
    console.log(json);
    return json;
  } catch (e) {
    return e.message;
  }
}
