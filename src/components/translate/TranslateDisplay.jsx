import { useSelector } from "react-redux";
import styles from "./Translate.module.css";

export default function TranslateDisplay(props) {
  const translatorCollection = useSelector((state) => state.translator);
  const specialChars = /[^A-Z0-9]/i;

  return (
    <div className={styles.Translated}>
      {translatorCollection.current.length > 1 &&
        translatorCollection.current.split("").map((char, index) => {
          if (char.match(specialChars)) {
            return null;
          }
          return (
            <img
              src={`resources/individial_signs/${char}.png`}
              key={index}
              alt={char}
            ></img>
          );
        })}
    </div>
  );
}
