import { Grid } from "semantic-ui-react";
import styles from "./Translate.module.css";
import { useState } from "react";
import TranslateDisplay from "./TranslateDisplay";
import { useDispatch } from "react-redux";
import {
  addTranslationToUser,
  setCurrentTranslation,
} from "../../redux/actions";

export default function Translate() {
  const dispatch = useDispatch();
  const [translationText, setTranslationText] = useState("");

  function onTextChanged(e) {
    setTranslationText(e.target.value);
  }

  function onTranslateClicked() {
    dispatch(addTranslationToUser(translationText));
    dispatch(setCurrentTranslation(translationText));
  }
  return (
    <Grid centered>
      <Grid.Row>
        <div className={styles.InputField}>
          <input
            placeholder="Text to translate!"
            value={translationText}
            onChange={onTextChanged}
            className={styles.Input}
          ></input>
          <button
            onClick={onTranslateClicked}
            className={styles.TranslateButton}
          >
            <span class="material-icons">keyboard_arrow_right</span>
          </button>
        </div>
      </Grid.Row>
      <Grid.Row>
        <TranslateDisplay />
      </Grid.Row>
    </Grid>
  );
}
