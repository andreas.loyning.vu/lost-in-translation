import Login from "./components/login/Login";
import Translate from "./components/translate/Translate";
import AppHeader from "./shared/AppHeader";
import NotFound from "./shared/NotFound";
import User from "./components/userProfile/User";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { requireAuth } from "./requireAuth";

export default function App(store) {
  return (
    <Router>
      <AppHeader />
      <Switch>
        <Route exact path="/" component={Login} />
        <Route
          path="/translator"
          render={() => requireAuth(store, Translate)}
        ></Route>
        <Route path="/profile" render={() => requireAuth(store, User)}></Route>
        <Route path="*/:path" component={NotFound}></Route>
      </Switch>
    </Router>
  );
}
