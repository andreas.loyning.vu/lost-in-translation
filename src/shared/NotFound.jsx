import { useParams } from "react-router-dom";
export default function NotFound() {
  const { path } = useParams();
  return (
    <main>
      <h1>Could not find the page {path}</h1>
    </main>
  );
}
