import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Grid } from "semantic-ui-react";
import styles from "./AppHeader.module.css";

export default function AppHeader() {
  const user = useSelector((state) => state.user);
  return (
    <div className={styles.MainDiv}>
      <Grid columns={3} className={styles.Banner}>
        <Grid.Column floated="left" className={styles.Content}>
          <Link to="/">
            <h2>Lost in Translation</h2>
          </Link>
        </Grid.Column>
        {user.id && (
          <Grid.Column floated="right" className={styles.Content}>
            <span className={styles.UserName}>{user.name}</span>
            <Link to="/profile" className={styles.Image}>
              <img
                src="resources/default.png"
                alt="profilePicture"
                className={styles.ProfilePicture}
              />
            </Link>
            <Link to="/translator">
              <button className={styles.TranslatorButton}>Translator</button>
            </Link>
          </Grid.Column>
        )}
      </Grid>
    </div>
  );
}
